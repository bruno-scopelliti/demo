import json
from flask import Flask, request, Response

app = Flask(__name__)

@app.route('/', methods=['GET'])
def respond_get():
    with open('data/ranking.json', 'r+') as f:
        data = json.load(f)
    return data


@app.route('/webhook', methods=['POST'])
def respond_hook():
    print(request.json)

    if request.json.get("comment"):
        on_comment_posted(request)
    elif request.json["pullrequest"]["state"] == "MERGED":
        on_pullrequest_merged(request)

    return Response(status=200)


def on_comment_posted(request):
    with open('data/teams.json', 'r+') as f:
        teams = json.load(f)

    with open('data/ranking.json', 'r+') as f:
        ranking = json.load(f)

    comment_author_id = request.json["comment"]["user"]["account_id"]
    team = get_team(teams, comment_author_id)
    if team:
        ranking[team] += 1
    
    with open('data/ranking.json', 'w+') as f:
        json.dump(ranking, f)


def on_pullrequest_merged(request):
    with open('data/teams.json', 'r+') as f:
        teams = json.load(f)

    with open('data/ranking.json', 'r+') as f:
        ranking = json.load(f)

    participants = request.json["pullrequest"]["participants"]

    for participant in participants:
        if participant["approved"]:
            partecipant_id = participant["user"]["account_id"]
            team = get_team(teams, partecipant_id)
            if team:
                ranking[team] += 2
    
    with open('data/ranking.json', 'w+') as f:
        json.dump(ranking, f)

def get_team(teams, participant_id):
    for team in teams.keys():
        if participant_id in [member['id'] for member in teams[team]]:
            return team
